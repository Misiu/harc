from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
      (r'^$', 'harcowanie.views.index'),
      (r'^szlaki/$', 'harcowanie.views.paths'),
      (r'^szlak/(?P<path_id>\d+)', 'harcowanie.views.path'),
      (r'^miejsce/(?P<location_id>\d+)', 'harcowanie.views.location'),
      (r'^miejsca/$', 'harcowanie.views.locations'),
      (r'^konto/$', 'harcowanie.views.account'),
      (r'^register/(?P<problems>\d+)', 'harcowanie.views.register'),
      (r'^register/$', 'harcowanie.views.register'),
      (r'^admin/$', include(admin.site.urls)),
      (r'^login/$', 'harcowanie.views.logging'),
      (r'^logout/$', 'harcowanie.views.logouting'),
      url(r'^ajax_calls/citySearch/', 'harcowanie.views.citySearch'),
)

