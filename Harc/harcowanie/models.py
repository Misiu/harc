from django.db import models
from django.conf import settings
# Create your models here.

# Kocham Misia! ;-*
# A Mis Lalę <3

class UserProfile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    birth_date = models.DateField()
    telephone = models.CharField(max_length=30, null=True)

class Permission(models.Model):
    name = models.CharField(max_length=30)
    
class Achievment(models.Model):
    name = models.CharField(max_length=30)
    permission = models.ManyToManyField(Permission, null=True)


class Path_type(models.Model):
    name = models.CharField(max_length=30)     
       
class Path(models.Model):
    name = models.CharField(max_length=30)
    type = models.ForeignKey(Path_type, null=True)
    description = models.CharField(max_length=700)
    

class Location_type(models.Model):
    name = models.CharField(max_length=30)
 
class District(models.Model):
    name = models.CharField(max_length=30) 
       
class Voivodeship(models.Model):
    name = models.CharField(max_length=30)   
     
class City(models.Model):
    name = models.CharField(max_length=30)
    district = models.ForeignKey(District)
    voivodeship = models.ForeignKey(Voivodeship, null=True)
    description = models.CharField(max_length=300, null=True)
        
        
class Location(models.Model):
    name = models.CharField(max_length=80)
    path = models.ForeignKey(Path, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    district = models.ForeignKey(District, null=True)
#     place_description= models.CharField(max_length=300, null=True)
    city = models.ForeignKey(City, related_name='city')

    city_second = models.ForeignKey(City, null=True, related_name='city_second')
    location_type = models.ForeignKey(Location_type, null=True)
    description = models.CharField(max_length=700)
    lantitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    event = models.ManyToManyField('Event', null=True, related_name='+')
    
    '''co to jest lan i lon? '''
# lan i lon
class Event(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    location = models.ManyToManyField('Location', null=True, related_name='+')
    date_start=models.DateField()
    date_end = models.DateField(null=True)
    description = models.CharField(max_length=700)
    
    
class Comments(models.Model):
    
    text = models.CharField(max_length=300)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    date_added = models.DateTimeField()
    location = models.ForeignKey(Location)
    path = models.ForeignKey(Path, null=True)
    
class Mark(models.Model):
    value = models.IntegerField()
    
class Photo(models.Model):
    name = models.CharField(max_length=30)
    image = models.ImageField(upload_to='/', max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    mark = models.ForeignKey(Mark)
    location = models.ForeignKey(Location)
    
    
    '''A nie lepiej dynamicznie wyszukiwac najaktywniejszych uzytkownikow?'''


    
    
class News(models.Model):
    title=models.CharField(max_length=30)
    content=models.CharField(max_length=500)
    
