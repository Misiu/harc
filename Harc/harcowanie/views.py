
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from harcowanie.example_inputs import *
from harcowanie.models import *
import datetime
import json

# Create your views here.
def index(request):
    example_inser_news()
    try: 
        all_news_from_database = News.objects.all()
    except all_news_from_database.DoesNotExist:
        raise Http404
    
    return render_to_response('index.html', {'all_news': all_news_from_database}, RequestContext(request))
   

                      
                                   
def paths(request):
        example_instert_paths()
    #try:
        if(request.POST):
            query = request.POST.get('nazwa', '')
            all_paths_from_database = Path.objects.filter(name__contains=query)
        else:
            all_paths_from_database = Path.objects.all()
   # except all_paths_from_database.DoesNotExist:
    #    raise Http404
        return render_to_response('paths.html', {'all_paths': all_paths_from_database}, RequestContext(request))  

    # return render_to_response('szlaki.html', {'all_paths': all_paths_from_database},  RequestContext(request))  
    
def new_location(request):
    if(request.POST):
        query = request.POST.get('add_location', '')
        
def locations(request):   
    insert_district()
    insert_location_type()
    insert_city()
    example_insert_location()

    try:
        if(request.POST):
            query = request.POST.get('nazwa', '')
            locations_from_database = Location.objects.filter(name__contains=query)
        else:
            locations_from_database = Location.objects.all()
    except locations_from_database.DoesNotExist:
        raise Http404

    return render_to_response('locations.html', {'locations': locations_from_database}, RequestContext(request))

def account(request):
    # example_insert_user()
    try:
        users_from_database = User.objects.all()
        districts=['dolnośląskie','lubelskie', 'lubuskie', 'łódzkie' ,'kujawsko-pomorskie', 'małopolskie','mazowieckie','opolskie','podkarpackie', 'pomorskie','podlaskie','śląskie','świętokrzyskie','warmińsko-mazurskie','wielkopolskie', 'zachodniopomorskie']
    except users_from_database.DoesNotExist:
        raise Http404
    
    return render_to_response('account.html', {'users': users_from_database, 'districts':districts}, RequestContext(request))    
    

    
def logging(request): 
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect(request.META['HTTP_REFERER'], context_instance=RequestContext(request))
        else:
            pass
            # Return a 'disabled account' error message
    else:
        pass
        # Return an 'invalid login' error message.
        
    return redirect(request.META['HTTP_REFERER'], context_instance=RequestContext(request))

def logouting(request):
    logout(request)
    # Redirect to a errors page.
    return redirect(request.META['HTTP_REFERER'], context_instance=RequestContext(request))


def register(request, problems=0):
    if(problems==0):
        return render_to_response('register.html', RequestContext(request))
    errors = []
    if(request.POST.get('password', 1) != request.POST.get('confirm_password', 2)):
        errors.append('wrong_password')
    if(request.POST.get('username', "field is empty") == "" or request.POST.get('email', "field is empty") == "" or request.POST.get('password',"field is empty") == ""):
        errors.append('empty_field')
    if(len(errors) != 0):
        return render_to_response('register.html', {'errors': errors}, RequestContext(request))  
    else:   
        User.objects.create_user(username=request.POST['username'], email=request.POST['email'], password=request.POST['password'])     
        user=authenticate(username=request.POST['username'], password=request.POST['password'])
        login(request, user)
        return render_to_response('index.html', RequestContext(request)) 
    
def location(request, location_id):
        #  try:
        miejsce = Location.objects.get(id=location_id)
        if(request.POST):
            query = request.POST['komentarz']
            temp = Comments(text=query, user=request.user, date_added=datetime.datetime.now(), location=Location.objects.get(id=location_id))
            temp.save()
            #         try:
        comments = Comments.objects.filter(location=location_id)
            # except:
            #             pass
        # except:
        #     raise Http404
    

        return render_to_response('location.html', {'miejsce': miejsce, 'comments':comments}, RequestContext(request))  
# user=active user
#
def path(request, path_id):
    
    path = Path.objects.get(id=path_id)
    if(request.POST):
            query = request.POST['komentarz']
            temp = Comments(text=query, user=request.user, date_added=datetime.datetime.now(), location=Location.objects.get(id=path_id))
            temp.save()
    comments = Comments.objects.all()
    return render_to_response('path.html', {'path':path, 'comments':comments}, RequestContext(request))

def comment(request):

        if(request.POST):
            query = request.POST.get('nazwa', '')
            temp = Comments(text=query, user='lala', date_added=datetime.datetime.now(), location=Location.objects.get(id=id))
            temp.save()
            locations_from_database = Location.objects.all()
            #   locations_from_database = Location.objects.filter(location_name__contains=query)
        else:
            locations_from_database = Location.objects.all()
            #   except locations_from_database.DoesNotExist:
            #       raise Http404
        
            # return render_to_response('miejsce.html', {'comments': query},  RequestContext(request))
    
    
def citySearch(request):
    if request.is_ajax():
        #term to zapytanie wyslane przez jQuery
        query = request.GET.get('term', '')
        cities = City.objects.filter(name__contains=query)
        results=list()
        for city in cities:
            results.append(city.name)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

