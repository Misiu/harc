'''
Created on 21-09-2014

@author: Vardens
'''
from harcowanie.models import *
def example_inser_news():
    if(len(News.objects.all())>=3):
        return
    temp = News(title='News pierwszy',
        content='''
        W pokoiku, przy grzejniku
        leży misiu na stoliku.
        Wciąż mu myśli krążą w głowie
        kto przytuli dziś go sobie.
        Miś do ciebie puszcza oczka
        chce noszony być na rączkach.
        Przytul misia spraw mu radość.
        Miś odwdzięczy się zabawą.'''
         ) 
    temp.save()
    temp = News(title='News drugi',
        content='''Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco 
        laboris nisi ut aliquip ex ea commodo consequat. 
        Duis aute irure dolor in reprehenderit in voluptate velit esse 
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
        cupidatat non proident, sunt in culpa qui officia deserunt mollit 
        anim id est laborum.'''
         ) 
    temp.save()  
    temp = News(title='News trzeci',
        content='''Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco 
        laboris nisi ut aliquip ex ea commodo consequat. 
        Duis aute irure dolor in reprehenderit in voluptate velit esse 
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
        cupidatat non proident, sunt in culpa qui officia deserunt mollit 
        anim id est laborum.'''
         ) 
    temp.save()    
    
    
def example_instert_paths():
    if(len(Path.objects.all())>=3):
        return
    temp = Path(name='Szlak wygasłych wulkanów', description='Trasa ma ok 80 km\n rozpoczyna się w Jaworze a kończy w Świdnicy')
    temp.save()
    temp = Path(name='Szlak mnichów cysterskich', description='Zaczyna się w Męcicne niedalko dawnego opactwa i idzie do Lublina')
    temp.save()
    temp = Path(name='Szlak zielony Jawor-Świdnica', description='Niebywała okazja przyjrzec się wszystkim Jaworom po drodze do Jawora\n Historia głosi ze spacerowala tedy św. Jadwiga')
    temp.save()
    
def insert_district():
    if len(District.objects.all())==0:       
        temp = District(name='dolnośląskie')
        temp.save()
        temp = District(name='opolskie')
        temp.save()
        
def insert_city():
    if(len(City.objects.all())>=2):
        return
    temp = City(name='Jawor', district=District.objects.get(name='dolnośląskie'), description='Malownicze miasto słynące z targów chleba i Kościoła Pokoju')    
    temp.save()
    temp = City(name='Szklarska Poręba', district=District.objects.get(name='dolnośląskie'), description='Górski kurort, na drodze najpiękniejszych szlaków Karkonoszy')
    temp.save()
    
def insert_path_type():
    if len(Path_type.objects.all()) == 0:
        temp=Path_type(name='edukacyjny')
        temp.save()    
        temp=Path_type(name='turystyczny')
        temp.save()    
        temp=Path_type(name='krajobrazowy')
        temp.save()    
        temp=Path_type(name='rowerowy')
        temp.save()  
      
def insert_location_type():
    
    if len(Location_type.objects.all()) == 0:
        temp=Location_type(name='szczyt')
        temp.save()    
        temp=Location_type(name='skały')
        temp.save()    
        temp=Location_type(name='punkt widokowy')
        temp.save()    
        temp=Location_type(name='budynek')
        temp.save()    
    
# def example_insert_user():
#     #insert_location_type()
#     #insert_path_type()
#     if(len(User.objects.all())>=3):
#         return            
#     temp = User(nick_name='Karoluch', first_name='Karo', last_name='Her', password='lala', email='lola@o2.pl')
#     temp.save()
#     temp = User(nick_name='luch', first_name='Karo', last_name='Her', password='lala99', email='Marika@o2.pl')
#     temp.save()
#     temp = User(nick_name='Ach', first_name='Karo', last_name='Her', password='lala99ppp', email='Porika@o2.pl')
#     temp.save()
    
def example_insert_location():    
    if(len(Location.objects.all()) >= 2):
        return
    temp = Location(name='Wysoki Kamień', description='Ciekawostką jest, że obecnie właścicielem szczytu Wysoki Kamień jest osoba prywatna, Paweł Gołba. Jest to przypadek precedensowy, żeby właścicielem szczytu została osoba prywatna.\nW tej chwili pan Gołba kończy budowę nowego schroniska na Wysokim Kamieniu. Sezonowo otwarty jest bufet.',
                path=Path.objects.get(name='Szlak wygasłych wulkanów'), city=City.objects.get(name='Szklarska Poręba'))
                
    temp.save()
    
    '''to ponizej odkomentuj i znajdz blad'''
#     temp = Location(location_name='Białe skały', description='Znajdują się w drodze na Wysoki Kamień, gdy odejdzie się ze szlaku, gdy za wiaduktem wejdziemy w las.',
#                 path_id=Path.objects.get(path_name='Szlak wygasłych wulkanów', city=City.objects.get(name='Szklarska Poręba'))
#                 )
#     temp.save() 
    